# developers - additional
tox
pytest>=6.2.4
pytest-cov
flake8~=4.0.1
pytest-flake8
setuptools>=56.0.0
sphinx

lzss~=0.3
Pillow~=9.3.0