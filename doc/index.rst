.. sm2mpx10 documentation master file, created by
   sphinx-quickstart on Tue Sep 27 17:16:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sm2mpx10's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api
   usage/tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
